# Xrandr - monitor switcher

## Info

Script to switch monitors with xrandr. The original script was found at arch linux wiki, see link below.

https://wiki.archlinux.org/title/Xrandr#Toggle_external_monitor

## How to use

´´´
/.monitor-switcher.sh
´´´

## Safe to run?

Be sure to have a strategy for switching back to your standard monitor if somethings goes wrong :)
