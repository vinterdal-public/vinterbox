#!/bin/sh

# original script from: https://wiki.archlinux.org/title/Xrandr#Toggle_external_monitor

# Thinkpad T450s
# eDP-1     laptop monitor
# DP-2      vga

intern=eDP-1
extern=DP-2

if xrandr | grep "$extern disconnected"; then
    xrandr --output "$extern" --off --output "$intern" --auto
else
    xrandr --output "$intern" --off --output "$extern" --auto
fi
