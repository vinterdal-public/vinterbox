#!/bin/bash

echo ":: Starting update with apt"
apt-get update && apt-get upgrade -y
echo ":: Update finished"

apps=(apt-transport-https curl git htop links vim ncdu python3 sudo tmux watch wget)

echo ":: Installing ${apps[*]}"

apt-get install ${apps[*]} -y
