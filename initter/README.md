# initter

## Info

Use if you don't manage your clients with a tool like ansible and just want to quickly update the operatingsystem and install wanted packages.

## Safe to run?

* this script will install all updates available
* this script will install all desired packages


Minimum version of bash is x.x

## How to use
```
$ ./initter-apt.sh
```

## TODO
- [ ] Create initter-yum.sh so that such systems can use it aswell.

## Compability

Linux systems with APT.
