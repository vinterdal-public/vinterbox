# scrap

## Info

Enter command scrap in a shell and drop into $EDITOR and begin typing.  
A directory will be created automaticly and a file will be prepared for you.  

This makes it easy to quickly bring up an editor and save files in a somewhat ordered manner. It is easy to then search for you stuff with grep or any other tool, think: **grep "note" -i -r ~/scrap/**  

### Structure:  
**Example:** ~/scrap/50-2020/ek8r-1455  
**scrap** = base directory  
**50** = week number  
**2020** = year  
**ek8r** = 4 character long random a-z0-9  
**1455** = 24 hour time, 14:55  

There exists one userinput. When starting scrap, you are able to give an description which will be added to the top of the file like this:
```
# this is my description
this is what I want to write for my note
```
It is also possible to skip description by pressing **Enter**.

## How to use  
Place the script in one of your entries of $PATH. On Ubuntu 20.04 I've used **/usr/local/bin**  
Set is as executable:
```
chmod +x /usr/local/bin/scrap
```

Run in your shell:
```
$ scrap
```

## Safe to run?  
Plain files are created in your $HOME

## Compability
Probably most shells with $EDITOR that is capable of __$EDITOR filname"__, as well as presence of $HOME.
