# fdupes parser

## Info

Convert result from **fdupes** to json

## How to use  

```
fdupes . | python3 fdupes_parser.py
```

## Safe to run?  

Yes

## Compability

You need Python version 3
