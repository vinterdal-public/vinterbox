import json
import sys

result = {}
counter = 0
section_state = True

lines = sys.stdin.read().splitlines()

for line in lines:
    if line == "" and counter == 0:
        counter += 1
        result[str(counter)] = []
    elif line == "":
        section_state = not section_state
        if section_state:
            counter += 1
            result[str(counter)] = []
    elif section_state and str(counter) in result.keys():
        result[str(counter)].append(line)

print (json.dumps(result))
