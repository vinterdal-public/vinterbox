$PATH = "c:\temp\test2.dat"
$SIZE_MB = [int64](300MB)

if (!(Get-Item -Path $PATH))
{
    Write-Warning ("The path '{0}' does not exists." -f $PATH)
    $user_input = Read-Host ("Do you want to create the necessary directories for the path? (y/N)")
    if ($user_input.ToUpper() -eq 'Y')
    {
        New-Item -Path $PATH -ItemType Directory
    }
}

$f = New-Object System.IO.FileStream $PATH, Create, ReadWrite
$f.SetLength($SIZE_MB)
$f.Close()
