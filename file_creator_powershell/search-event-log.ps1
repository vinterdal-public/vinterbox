$PATTERN = "Power"

# Run through only LogName "Application"
Get-EventLog -LogName Application | Select-Object Message | Select-String -Pattern $PATTERN

# Run through all available LogName(s)
Get-EventLog -LogName * | Select-Object Log | %{
    Get-EventLog -LogName $_.Log | Select-Object Message | Select-String -Pattern $PATTERN
}
