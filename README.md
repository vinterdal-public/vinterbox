# vinterbox

This project contains various tools and scripts we hope people find useful.

## Info

Each tool lives in it's own directory and should contain a README.md. Always read them as they contain information about the status of the tool and if it may or may not perform __destructive__ actions or any other unforeseen consequences.

