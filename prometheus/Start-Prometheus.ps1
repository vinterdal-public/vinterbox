<#
This script will simply start prometheus hidden.
To interact with the process use `Get-Process -ProcessName "prometheus"`

INPUTS:
 - Set $script to the correct path for your Start-Prometheus.ps1

TODO:
 - Connect to EventLog
#>

$executable_full_path = "C:\XXX\YYY\prometheus.exe"

try {
    $p = Get-Item $executable_full_path -ErrorAction Stop
} catch {
    #Write-EventLog -LogName "Application" -Source "Prometheus" -EventId 1000 -EntryType Error -Message "Could not start Prometheus"
    Write-Warning -Message ("Could not start Prometheus!")
    Write-Output $_
}

Start-Process -FilePath $p.FullName -WorkingDirectory $p.Directory -WindowStyle Hidden
