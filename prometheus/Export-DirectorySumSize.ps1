<#
EXAMPLE
CreatePromTextFile -Name "name of metric" -Description "Summed size of directory contents" -Textfile_inputs "C:\Program Files\windows_exporter\textfile_inputs" -Folders @('C:\TEMP', 'D:\Filearea\blabla') -Recurse
#>

function CreatePromTextFile {
    [CmdletBinding()]

    param (

        [Parameter(Mandatory)]
        [string]
        $Name,

        [Parameter(Mandatory)]
        [string]
        $Description,

        [Parameter(Mandatory)]
        [string]
        $Textfile_inputs,

        [Parameter(Mandatory)]
        [string[]]
        $Folders,

        [Parameter()]
        [switch]
        $Recurse
    )

    $prom_textfile = Join-Path -Path $Textfile_inputs -ChildPath ("{0}.prom" -f $Name)

    Set-Content -Path $prom_textfile -Encoding Ascii -NoNewline -Value ""
    Add-Content -Path $prom_textfile -Encoding Ascii -NoNewline -Value ("# HELP {0} {1}`n" -f $Name, $Description)
    Add-Content -Path $prom_textfile -Encoding Ascii -NoNewline -Value ("# TYPE {0} counter`n" -f $Name)

    foreach ($folder in $Folders) {
        # Backslash makes windows_exporter not happy
        $folder_formatted = '{0}{1}{0}' -f '"', $folder.Replace('\', '/')
        
        if ($Recurse) {
            $sum_bytes = (Get-ChildItem -Path $folder -Recurse | Measure-Object -Property Length -Sum | Select-Object Sum).sum
            Add-Content -Path $prom_textfile -Encoding Ascii -NoNewline -Value "${Name}{windows_directory_recurse=${folder_formatted}} ${sum_bytes}`n"
        } else {
            $sum_bytes = (Get-ChildItem -Path $folder | Measure-Object -Property Length -Sum | Select-Object Sum).sum
            Add-Content -Path $prom_textfile -Encoding Ascii -NoNewline -Value "${Name}{windows_directory=${folder_formatted}} ${sum_bytes}`n"
        }
    }
}
