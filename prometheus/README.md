# Prometheus and windows_exporter

## Info

This is a collection of scripts to ease use of prometheus and windows_exporter on Windows server.

## Export-DirectorySumSize.ps1

Collect a total sum of contents of a directory, recursive or single.
It's a bit messy and mostly a skelleton for creating .prom-files for windows_exporter

## Create scheduled task and start prometheus

1. Open **Start-Promethues.ps1** and set `$executable_full_path` to your prometheus executable.

2. Open **Create-ScheduledTask.ps1** and set `$script` to point to `Start-Prometheus.ps1`

3. Run **Create-ScheduledTask.ps1** to install (register) the task into TaskScheduler

4. Restart server and use `Get-Process -ProcessName "prometheus"` to see if it's running

