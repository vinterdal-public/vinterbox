<#
This script will create a scheduled task that is set to be run at startup with user "System"

INPUTS:
 - Set $script to the correct path for your Start-Prometheus.ps1
#>

$script = "C:\XXX\YYY\Start-Prometheus.ps1"

# Action
$ts_action_argument = ("-file {0}" -f $script)
$ts_action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument $ts_action_argument

# Trigger
$ts_trigger = New-ScheduledTaskTrigger -AtStartup -RandomDelay (New-TimeSpan -Seconds 30)

# Settings
$t = New-TimeSpan -Days 9999
$ts_settings = New-ScheduledTaskSettingsSet -ExecutionTimeLimit $t

# Task
$ts = New-ScheduledTask -Action $ts_action -Trigger $ts_trigger -Settings $ts_settings
Register-ScheduledTask -InputObject $ts -TaskName "Prometheus starter" -TaskPath '\Prometheus\' -User "System"
