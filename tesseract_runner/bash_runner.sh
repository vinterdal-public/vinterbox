#!/bin/bash

# Varaibles to set: IMAGE


IMAGE=""
LANGUAGES=( eng swe )
DATE_START_EXECUTION=$(date +"%Y-%m-%d_%H-%M-%S")
OUTPUT_DIRECTORY="./tesseract_results/$DATE_START_EXECUTION"

printf "\n:: Creating directory: $OUTPUT_DIRECTORY"
mkdir -p $OUTPUT_DIRECTORY

printf "\n:: Starting loop"
for i in `seq 0 13`
do
    printf "\n:: running tesseract with '--psm $i'"
    for lang in "${LANGUAGES[@]}"
    do
        printf "\n:: running tesseract with '-l $lang'"
        f_name="$OUTPUT_DIRECTORY/$lang-$i.txt"
        time tesseract $IMAGE $f_name --psm $i -l $lang
    done
done

