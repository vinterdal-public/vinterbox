# Aliases / shortcuts

## public ip

Get public ip

```
alias myip='curl https://ifconfig.io/ip'
```

## Alphabet

Print the english alphabet

```
alias alpha='echo {A..Z}'
```
